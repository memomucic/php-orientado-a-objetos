<?php
class token{
    //declaracion de atributos
    private $nombre;
    private $token;
    //declaracion de metodo constructor
    public function __construct($nombre_front){
        $this->nombre=$nombre_front;
        for ($a=0;$a<4;$a++) //hice un ciclo for para asignar 4 veces la letra aleatoria
        {
            $this->token=$this->token.chr(rand(ord('A'), ord('Z')));
        }




        
    }

    //declaracion del metodo mostrar para armar el mensaje con el nombre y token
    public function mostrar(){
        return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
    }

    //declaracion de metodo destructor
    public function __destruct(){
        //destruye token
        $this->token='El token ha sido destruido';
        echo $this->token; //La impresion del token destruido no puede mostrar nada ya que se ha liberado la memoria 
    }
}
?>